##### prepare
```
composer install
```
##### run tests
```
vendor/phpunit/phpunit/phpunit /app/test/
```
##### run demo
```
php -S 0.0.0.0:80 -t public/
```
##### open in browser
* <http://127.0.0.1/?id=5&from=daemon>
* <http://127.0.0.1/?id=5&from=mysql>
* <http://127.0.0.1/?id=5&from=bla> - error
* <http://127.0.0.1/> - error

Об архитектуре:

- точкой входа является public/index.php

- легаси функции расположены в legacy/functions.php

- для логирования создано две реализцаии интерфейса ILogger (плюс реализция логирующая в массив для упрощения тестов - LoggerMock)

- Logger - врапер над гипотетическим FileLogger

- LoggerNoop - реализация отключающая логирование (Null object паттерн)

- выбор между Logger и LoggerNoop осуществляется на основе переменной окружения LOG_ENABLED

- для представления данных объявления реализован класс AdValueObject

- AdValueObject инстанцируется через фабричные методы класса AdFactory в зависимости от источника данных

- для преобразования AdValueObject в html используется шаблонизатор AdDecorator

- для конвертации долларов в рубли создан класс CurrencyConverter, который является зависимостью AdDecorator и хранит текущий курс валюты

- посредником для запуска всей бизнес логики является класс AdMediator

- ядро приложения - класс Core отвечает за разбор запроса, запуск посредника и обработку ошибок