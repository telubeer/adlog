<?php
/**
 *
для запроса /?id=1&from=mysql
ответ:
    <h1>AdName_FromMySQL</h1>
    <p>AdText_FromMySQL</p>
    <p>стоимость: Х руб</p>

для запроса /?id=1&from=daemon
ответ:
    <h1>AdName_FromDaemon</h1>
    <p>AdText_FromDaemon</p>
    <p>стоимость: Х руб</p>
 * */
define('ROOT_PATH', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);
require_once ROOT_PATH . 'vendor/autoload.php';

$log = getenv('LOG_ENABLED') ? new \App\Logger() : new \App\LoggerNoop();
define('CURRENT_RATE', 65.55);
new \App\Core(
    new \App\AdMediator($log),
    new \App\AdDecorator(new \App\CurrencyConverter(CURRENT_RATE))
);

try {
    echo \App\Core::instance()->preview($_GET['id'] ?? null, $_GET['from'] ?? null);
} catch (Exception $e) {
    echo 'error :/';
}
die(0);