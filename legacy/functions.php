<?php

/**
 * 1. Получение данных об объявлении из базы
 * @param $id
 * @return array
 */
function getAdRecord($id)
{
    // пример ответа
    return [
        'id'       => $id,
        'name'     => 'AdName_FromMySQL',
        'text'     => 'AdText_FromMySQL',
        'keywords' => 'Some Keywords',
        'price'    => 10, // 10$
    ];
}


/**
 * 2. Получение данных об объявлении из кеширующего демона
 * @param $id
 * @return string
 */
function get_deamon_ad_info($id)
{
    // пример ответа: строка разделенная табуляцией
    return "{$id}\t235678\t12348\tAdName_FromDaemon\tAdText_FromDaemon\t11";
}