<?php

namespace App;
class CurrencyConverter
{
    protected $currentRate;

    public function __construct($currentRate)
    {
        $this->currentRate = $currentRate;
    }

    public function convert($value)
    {
        return number_format($value * $this->currentRate, 2, ',', ' ');
    }
}