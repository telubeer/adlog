<?php

namespace App;

/**
 * Class Core Singleton
 * @package App
 */
class Core {
    /**
     * @var AdMediator
     */
    protected $mediator;
    /**
     * @var AdDecorator
     */
    protected $decorator;

    /**
     * @var Core
     */
    protected static $instance;

    /**
     * @return Core
     * @throws Exception
     */
    public static function  instance() {
        if (is_null(static::$instance)) {
            throw new Exception('core not found');
        }
        return static::$instance;
    }

    public function __construct(AdMediator $mediator, AdDecorator $decorator)
    {
        $this->mediator = $mediator;
        $this->decorator = $decorator;
        static::$instance = $this;
    }

    /**
     * @param $id
     * @param $source
     * @return string
     * @throws BadRequest
     */
    public function preview($id, $source) {
        if (empty($id) || empty($source)) {
            throw new BadRequest();
        }
        $ad = $this->mediator->fetch($id, $source);
        return $this->decorator->decorate($ad);
    }
}