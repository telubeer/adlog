<?php

namespace App;
class AdDecorator
{
    protected $converter;
    public function __construct(CurrencyConverter $converter)
    {
        $this->converter = $converter;
    }

    /**
     * @param AdValueObject $ad
     * @return string
     */
    public function decorate(AdValueObject $ad)
    {
        return <<<EOF
<h1>{$this->escape($ad->name)}</h1>
<p>{$this->escape($ad->text)}</p>
<p>стоимость: {$this->converter->convert($ad->price)} руб</p>
EOF;
    }

    public function escape($value) {
        return htmlspecialchars($value);
    }
}
