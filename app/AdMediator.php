<?php
namespace App;
class AdMediator {
    const SOURCE_MYSQL = 'mysql';
    const SOURCE_DAEMON = 'daemon';
    protected $logger;
    public function __construct(ILogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $id
     * @param $source
     * @return AdValueObject
     * @throws BadRequest
     */
    public function fetch($id, $source) {
        $functionName = $this->mapFunction($source);
        $data = $this->invokeFetch($functionName, $id);
        $this->log($functionName, $id);
        return $this->createAd($data, $source);
    }

    /**
     * @param $functionName
     * @param $id
     * @return mixed
     */
    protected function invokeFetch($functionName, $id) {
        return $functionName($id);
    }

    /**
     * @param $functionName
     * @param $id
     */
    protected function log($functionName, $id) {
        $message = sprintf(
            '[%s] %s(ID=%d)',
            date(DATE_ISO8601),
            $functionName,
            $id
        );
        $this->logger->logger($message);
    }

    /**
     * @param $data
     * @param $source
     * @return AdValueObject
     */
    protected function createAd($data, $source) {
        return $source === static::SOURCE_MYSQL
            ? AdFactory::createFromDb($data)
            : AdFactory::createFromDaemon($data);
    }

    /**
     * @param $source
     * @return string
     * @throws BadRequest
     */
    protected function mapFunction($source) {
        switch ($source) {
            case static::SOURCE_MYSQL:
                return 'getAdRecord';
            case static::SOURCE_DAEMON:
                return 'get_deamon_ad_info';
            default:
                throw new BadRequest();
        }
    }
}