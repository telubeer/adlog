<?php
namespace App;
class AdFactory {
    /**
     * @param $array
     * @return AdValueObject
     */
    public static function createFromDb($array) {
        return new AdValueObject($array);
    }

    /**
     * "{$id}\t235678\t12348\tAdName_FromDaemon\tAdText_FromDaemon\t11";
     *     где колонки:
     *    0. id - объявления
     *    1. id - кампании
     *    2. id - пользователя
     *    3. название объявления
     *    4. текст объявления
     *    5. стоимость объявления в $
     * @param $value
     * @return AdValueObject
     */
    public static function createFromDaemon($value) {
        [$id, $campaign, $user, $name, $text, $price] = explode("\t", $value);
        return new AdValueObject(compact(AdValueObject::FIELDS));
    }
}
