<?php

namespace App;

class AdValueObject
{
    const FIELDS = ['id', 'campaign', 'user', 'name', 'text', 'keywords', 'price'];
    const INTEGER = ['id', 'campaign', 'user'];
    public $id;
    public $campaign;
    public $user;
    public $name;
    public $text;
    public $keywords;
    public $price;

    public function __construct($data)
    {
        foreach (static::FIELDS as $fieldName) {
            $value = $data[$fieldName] ?? null;
            $isInteger = in_array($fieldName, static::INTEGER);
            $this->{$fieldName} = $isInteger ? (int) $value : $value;
        }
    }
}