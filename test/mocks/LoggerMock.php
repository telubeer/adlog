<?php
namespace Test;
use App\ILogger;

class LoggerMock implements ILogger {
    protected $log = [];

    public function logger($message)
    {
        array_push($this->log, $message);
    }

    public function get(): array
    {
        return $this->log;
    }
}