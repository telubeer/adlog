<?php

use PHPUnit\Framework\TestCase;

class AdFactoryTest extends TestCase
{

    public function testCreateFromDb()
    {
        $data = [
            'id' => 1,
            'name' => 'AdName_FromMySQL',
            'text' => 'AdText_FromMySQL',
            'keywords' => 'Some Keywords',
            'price' => 10, // 10$
        ];
        $ad = \App\AdFactory::createFromDb($data);
        foreach (array_keys($data) as $field) {
            $this->assertEquals($data[$field], $ad->{$field});
        }
    }

    public function testCreateFromDaemon()
    {
        $data = "1\t235678\t12348\tAdName_FromDaemon\tAdText_FromDaemon\t11";
        $ad = \App\AdFactory::createFromDaemon($data);
        $fields = \App\AdValueObject::FIELDS;
        unset($fields['keywords']);
        $expected = [
            'id' => 1,
            'campaign' => 235678,
            'user' => 12348,
            'name' => 'AdName_FromDaemon',
            'text' => 'AdText_FromDaemon',
            'price' => 11,
        ];
        foreach ($fields as $field) {
            $this->assertEquals($expected[$field], $ad->{$field}, 'field = ' . $field);
        }
    }
}