<?php
require_once dirname(__FILE__) . '/mocks/LoggerMock.php';

use PHPUnit\Framework\TestCase;

class AdMediatorTest extends TestCase
{
    /**
     * проверяем что при вызове функции получения данных рекламного объявления
     * этот вызов логируется
     */
    public function testInvokeAndLog()
    {
        $logger = new \Test\LoggerMock();
        $adMediator = $this->getMockBuilder(\App\AdMediator::class)
            ->setConstructorArgs([$logger])
            ->setMethods(['invokeFetch'])
            ->getMock();
        // подменяем вызов функции получения объявления из базы
        $adMediator->expects($this->once())
            ->method('invokeFetch')
            ->will($this->returnValue([
                'id'       => 1,
                'name'     => 'AdName_FromMySQL',
                'text'     => 'AdText_FromMySQL',
                'keywords' => 'Some Keywords',
                'price'    => 10,
            ]));

        $adMediator->fetch(1, \App\AdMediator::SOURCE_MYSQL);
        $this->assertCount(1, $logger->get());
    }
}